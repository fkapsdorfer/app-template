/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This module provides a function for extending functions in multiple layer namespaces.
 */

'use strict'

var util = require('util');

module.exports = function recursiveExtend (prop, base) {
    if (prop instanceof Function) {
        // if function -> extend
        util.inherits(prop, base);
    } else if (prop instanceof Object) {
        // If namespace
        for (var key in prop) {
            if (prop.hasOwnProperty(key)) {
                var subProp = prop[key];
                recursiveExtend(subProp, base);
            }
        }
    }
}
