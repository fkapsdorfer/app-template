/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This file defines and exports objects used to identify and handle errors. All errors extends ErrorBase.
 */

'use strict'

var recursiveExtend = require('./recursiveExtend');


/**
 * @extends ErrorBase
 */
var Errors = {};



function ErrorBase () {
    // Do not use constructor - so it don't have to be called in the subclass

    // If should be logged
    // this.log = false;

    // Error from lower layer
    // this.err = undefined

    // This will be showed to the user
    // this.msg = 'An error happened.'

    // The http status to send
    // this.status = 500
}

/**
 * Sends the HTTP response and logs error.
 * @param {Response} res
 */
ErrorBase.prototype.handle = function(res) {
    if (this.log) {
        console.error(this);
        if (this.err) {
            console.trace(this.err);
        }
    }
    return res
        .status(this.status ? this.status : 500)
        .render('error.html', { msg: this.msg ? this.msg : 'An error happened.' });
}



Errors.Namespace = {
    YourError: function(err) {
        this.msg    = 'Your message goes here.';
        this.err    = err;
        this.log    = true;
    }
}



// Extend all error "classes" by BaseError
recursiveExtend(Errors.Namespace, ErrorBase);



module.exports = Errors;
