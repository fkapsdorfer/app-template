var assert  = require('assert');
var re      = require('../libs/recursiveExtend');


describe('recursiveExtend', function () {
    var Base = function () { this.prop = 'prop' };
    Base.prototype.baseMethod = function () {};

    var Ns = {
        Ns2: {
            Child: function () { Base.call(this) }
        }
    };

    re(Ns, Base);
    var obj = new Ns.Ns2.Child();

    it('Should extend Base', () => {
        assert.ok(obj instanceof Base);
    });
    it('Should be extended by prototype members', () => {
        assert.strictEqual(obj.baseMethod, Base.prototype.baseMethod);
    });
    it('Should be extended by base constructor members', () => {
        assert.strictEqual(obj.prop, 'prop');
    });
    it('Namespaces should not be extended', () => {
        assert.strictEqual(Ns.Ns2.prototype, undefined);
    });
});
