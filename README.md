
Document Structure
==================


Running
-------
```sh
NODE_ENV=[development/production] npm start
```


Certificate installation
------------------------
```sh
openssl req -x509 -newkey rsa:2048 -nodes -keyout server.key -out server.cert
```


Modules / Utilities
-------------------

### Included Modules
- expres
- morgan
- body-parser
- nunjucks
- async

### Other Useful Modules
- socket.io
- cookie-parser
- express-session
- passport
- passport-local
- bcrypt-nodejs
- nedb
- mongoose
- winston
- underscore

### Dev. Modules / Utilities
- rewire
- mocha
- gulp
- grunt
- jsdoc


Installing Dependencies
-----------------------

```sh
npm install
npm install --save
npm install --save-dev <package>
npm install -g <package>
npm uninstall --save <package>
```


```sh
npm run-script getAsset bootstrap
```
